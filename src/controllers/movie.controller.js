const logger = require('../config/appconfig').logger
const database = require('../datalayer/mssql.dao')

module.exports = {
  getAllMovies: (req, res, next) => {
    logger.info('Get /api/movies aangeroepen')

    const query = 'SELECT * FROM Movie;'
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows })
      }
    })
  },

  getMovieById: function(req, res, next) {
    logger.info('Get /api/movies/id aangeroepen')
    const id = req.params.movieId

    const query = 'SELECT * FROM Movie WHERE Movie.id=' + id
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows })
      }
    })
  },

  createMovie: function(req, res, next) {
    logger.info('Post /api/movies aangeroepen')
    // hier komt in het request een movie binnen.
    const movie = req.body
    // const postcodeValidator = new RegExp('')
    // if (!postcodeValidator.test(movie.postcode)) {
    //   // ongeldige input, return errormessage
    // }
    logger.info(movie)

    const query =
      "INSERT INTO Movie(MovieId, Title, Description, Year) VALUES (12,'" +
      movie.title +
      "','" +
      movie.description +
      "','" +
      movie.year +
      "')"
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows })
      }
    })
  }
}
